# Start with latest golang image
FROM golang:alpine as builder
# Select our working directory
WORKDIR /app
# Install the build tools we need
RUN apk add --no-cache make curl git build-base gcc g++ libc-dev binutils-gold
# Clone the project to working directory
RUN git clone https://github.com/kubernetes-sigs/descheduler.git .
#RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# VERSION is currently based on the last commit
RUN VERSION=$(git describe --tags) \
&& COMMIT=$(git rev-parse HEAD) \
&& BUILD=$(date +%FT%T%z) \
&& LDFLAG_LOCATION=sigs.k8s.io/descheduler/cmd/descheduler/app \
&& LDFLAGS="-ldflags \"-X ${LDFLAG_LOCATION}.version=${VERSION} -X ${LDFLAG_LOCATION}.buildDate=${BUILD} -X ${LDFLAG_LOCATION}.gitCommit=${COMMIT}\""

# Build the file
RUN GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build ${LDFLAGS} -o descheduler ./cmd/descheduler

# Start with new image
FROM alpine

# Set maintainer of this image
LABEL maintainer="admin@3pi.dev"
# Copy the binary
COPY --from=builder /app/descheduler /bin/descheduler
# Run
CMD ["/bin/descheduler", "--help"]
